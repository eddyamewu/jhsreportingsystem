using Microsoft.Practices.Unity;
using ReportGenerator.Data.Repository;
using ReportGenerator.Domain.Contracts;
using ReportGenerator.Domain.Sevices;

namespace ReportGeneratorTests.Infastructure
{
    public class TestBootStrapper
    {
        public static UnityContainer GetConatiner()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<ISchoolService, SchoolService>();
            container.RegisterType<ISubjectService, SubjectService>();
            container.RegisterType<IStudentService, StudentService>();
            return container;
        }
    }
}