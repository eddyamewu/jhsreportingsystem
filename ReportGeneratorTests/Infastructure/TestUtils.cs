using ReportGenerator.Domain.Models;
using System;

namespace ReportGeneratorTests.Infastructure
{
    public class TestUtils
    {
        public static School CreateSchool() => new School
        {
            Name = "SchoolName",
            TotalAttendance = 100,
            Contact = "1234567890",
            Year = DateTime.Now.Year,
            Address = "SchoolAddress",
            Location = "SchoolLocation",
            Resumption = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 3, 1),
            Closing = new DateTime(DateTime.Now.Year, DateTime.Now.Month + 3, 1),
        };

        public static Subject CreateSubject() => new Subject
        {
            ExamScore = 50,
            TestScore = 50,
            Name = "SubjectName",
            Teacher = "SubjectTeacher"
        };

        public static Student CreateStudent() => new Student
        {
            Form = "Basic 6",
            AttendanceMade = 70,
            Lastname = "StudentLastname",
            Firstname = "StudentFirstname",
            Othername = "StudentOthername",
            TeacherRemarks = "StudentTeacherRemarks",
            AcademicConduct = "StudentAcademicConduct",
            AcademicAttitude = "StudentAcademicAttitude",
            AcademicInterest = "StudentAcademicInterest",
            AcademicStrength = "StudentAcademicStrength",
            AcademicWeakness = "StudentAcademicWeakness",
            HeadmasterRemarks = "StudentHeadmasterRemarks"
        };

    }
}