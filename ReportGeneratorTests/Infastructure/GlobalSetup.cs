using ReportGenerator.Migrations;
using System.Data.Entity.Migrations;

namespace ReportGeneratorTests.Infastructure
{
    public class GlobalSetup
    {
        public static void MigrateDbToLatestVersion()
        {
            var configuration = new Configuration();
            var migrator = new DbMigrator(configuration);
            migrator.Update();
        }

    }
}