﻿using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReportGenerator.Domain.Contracts;
using ReportGeneratorTests.Infastructure;
using static System.String;

namespace ReportGeneratorTests.Domain.Sevices
{
    [TestClass]
    public class SchoolServiceTests
    {
        private static ISchoolService _schoolService;
        [ClassInitialize]
        public static void SetUp(TestContext context)
        {
            GlobalSetup.MigrateDbToLatestVersion();
            var container = TestBootStrapper.GetConatiner();
            _schoolService = container.Resolve<ISchoolService>();

        }
        [TestMethod]
        public void AddSchoolTest_WithValidModel_ReturnsIsSucceededIsTrue()
        {
            //Arrange
            var school = TestUtils.CreateSchool();

            //Act
            var result = _schoolService.AddSchool(school);

            //Assert
            Assert.IsTrue(result.IsSucceeded);
            Assert.AreSame(result.Message, "School saved successfully.");
        }

        [TestMethod]
        public void AddSchoolTest_WithInvalidModel_ReturnsIsSucceededIsTrue()
        {
            //Arrange
            var school = TestUtils.CreateSchool();
            school.Name = Empty;
            school.Contact = Empty;

            //Act
            var result = _schoolService.AddSchool(school);

            //Assert
            Assert.IsTrue(result.IsSucceeded);
            Assert.AreSame(result.Message, "School saved successfully.");
        }

        [TestMethod]
        public void AddSchoolTest_WithInvalidModelIsNull_ReturnsIsSucceededIsTrue()
        {
            //Arrange
            var school = TestUtils.CreateSchool();
            school.Name = null;
            school.Contact = null;

            //Act
            var result = _schoolService.AddSchool(school);

            //Assert
            Assert.IsTrue(result.IsSucceeded);
            Assert.AreSame(result.Message, "School saved successfully.");
        }
    }
}