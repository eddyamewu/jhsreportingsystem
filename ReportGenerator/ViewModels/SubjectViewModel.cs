﻿namespace ReportGenerator.ViewModels
{
    public class SubjectViewModel
    {
        public string Name { get; set; }
        public float TestScore { get; set; }
        public float ExamScore { get; set; }
        public string Remarks { get; set; }
        public string Teacher { get; set; }
    }

}
