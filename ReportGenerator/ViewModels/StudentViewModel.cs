﻿namespace ReportGenerator.ViewModels
{
    public class StudentViewModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Othername { get; set; }
        public string Form { get; set; }
        public bool Promoted { get; set; }
        public int AttendanceMade { get; set; }
        public string AcademicWeakness { get; set; }
        public string AcademicStrength { get; set; }
        public string AcademicConduct { get; set; }
        public string AcademicAttitude { get; set; }
        public string AcademicInterest { get; set; }
        public string TeacherRemarks { get; set; }
        public string HeadmasterRemarks { get; set; }
    }
}
