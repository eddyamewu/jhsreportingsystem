﻿using System;

namespace ReportGenerator.ViewModels
{
    public class SchoolViewModel
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public int Year { get; set; }
        public int TotalAttendance { get; set; }
        public DateTime Resumption { get; set; }
        public DateTime Closing { get; set; }
    }
}
