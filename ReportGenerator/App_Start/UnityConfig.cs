using Microsoft.Practices.Unity;
using ReportGenerator.Data.Repository;
using ReportGenerator.Domain.Contracts;
using ReportGenerator.Domain.Sevices;
using System.Web.Http;
using Unity.WebApi;

namespace ReportGenerator
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IUnitOfWork, UnitOfWork>(new HierarchicalLifetimeManager());
            container.RegisterType<ISchoolService, SchoolService>();
            container.RegisterType<ISubjectService, SubjectService>();
            container.RegisterType<IStudentService, StudentService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}