﻿using ReportGenerator.Domain.Models;
using ReportGenerator.ViewModels;

namespace ReportGenerator
{
    public class MappingConfig
    {
        public static void RegisterMaps()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<SchoolViewModel, School>();
                config.CreateMap<SubjectViewModel, Subject>();
                config.CreateMap<StudentViewModel, Student>();
            });
        }
    }
}
