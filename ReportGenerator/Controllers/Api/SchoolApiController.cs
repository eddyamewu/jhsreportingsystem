﻿using AutoMapper;
using ReportGenerator.Domain.Contracts;
using ReportGenerator.Domain.Models;
using ReportGenerator.ViewModels;
using System;
using System.Net;
using System.Web.Http;

namespace ReportGenerator.Controllers.Api
{
    [RoutePrefix("api/schools")]
    public class SchoolApiController : ApiController
    {
        private readonly ISchoolService _schoolService;

        public SchoolApiController() { }

        public SchoolApiController(ISchoolService schoolService)
        {
            _schoolService = schoolService;
        }

        [HttpGet]
        [Route("getSchools")]
        public IHttpActionResult GetSchools()
        {
            try
            {
                var result = _schoolService.GetSchools();
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("getSchool/{schoolId}")]
        public IHttpActionResult GetSchool(int schoolId)
        {
            try
            {
                var result = _schoolService.GetSchool(schoolId);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("addSchool")]
        public IHttpActionResult AddSchool(SchoolViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var school = Mapper.Map<SchoolViewModel, School>(viewModel);
                var result = _schoolService.AddSchool(school);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        [Route("updateSchool/{schoolId}")]
        public IHttpActionResult UpdateSchool(int schoolId, SchoolViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var school = Mapper.Map<SchoolViewModel, School>(viewModel);
                var result = _schoolService.UpdateSchool(schoolId, school);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);

            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete]
        [Route("deleteSchool/{schoolId}")]
        public IHttpActionResult DeleteSchool(int schoolId)
        {
            try
            {
                var result = _schoolService.DeleteSchool(schoolId);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }

        }

    }
}
