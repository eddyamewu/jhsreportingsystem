﻿using AutoMapper;
using ReportGenerator.Domain.Contracts;
using ReportGenerator.Domain.Models;
using ReportGenerator.ViewModels;
using System;
using System.Net;
using System.Web.Http;

namespace ReportGenerator.Controllers.Api
{
    [RoutePrefix("api/students")]
    public class StudentApiController : ApiController
    {
        private readonly IStudentService _serviceService;

        public StudentApiController() { }

        public StudentApiController(IStudentService serviceService)
        {
            _serviceService = serviceService;
        }

        [HttpGet]
        [Route("getStudents")]
        public IHttpActionResult GetStudents()
        {
            try
            {
                var result = _serviceService.GetStudents();
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("getStudent/{studentId}")]
        public IHttpActionResult GetStudent(int studentId)
        {
            try
            {
                var result = _serviceService.GetStudent(studentId);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("addStudent/{schoolId}")]
        public IHttpActionResult AddStudent(int schoolId, StudentViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var student = Mapper.Map<StudentViewModel, Student>(viewModel);
                var result = _serviceService.AddStudent(schoolId, student);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        [Route("updateStudent/{studentId}")]
        public IHttpActionResult UpdateStudent(int studentId, StudentViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);


                var student = Mapper.Map<StudentViewModel, Student>(viewModel);
                var result = _serviceService.UpdateStudent(studentId, student);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete]
        [Route("deleteStudent/{studentId}")]
        public IHttpActionResult DeleteStudent(int studentId)
        {
            try
            {
                var result = _serviceService.DeleteStudent(studentId);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

    }

}
