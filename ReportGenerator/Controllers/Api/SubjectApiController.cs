﻿using AutoMapper;
using ReportGenerator.Domain.Contracts;
using ReportGenerator.Domain.Models;
using ReportGenerator.ViewModels;
using System;
using System.Net;
using System.Web.Http;

namespace ReportGenerator.Controllers.Api
{
    [RoutePrefix("api/subjects")]
    public class SubjectApiController : ApiController
    {
        private readonly ISubjectService _subjectService;

        public SubjectApiController() { }

        public SubjectApiController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        [Route("getSubjects")]
        public IHttpActionResult GetSubjects()
        {
            try
            {
                var result = _subjectService.GetSubjects();
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("getSubject/{subjectId}")]
        public IHttpActionResult GetSubject(int subjectId)
        {
            try
            {
                var result = _subjectService.GetSubject(subjectId);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("addSubject/{studentId}")]
        public IHttpActionResult AddSubject(int studentId, SubjectViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var subject = Mapper.Map<SubjectViewModel, Subject>(viewModel);
                var result = _subjectService.AddSubject(studentId, subject);
                return Content(result.IsSucceeded ? HttpStatusCode.Created :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }

        }

        [Route("updateSubject/{studentId}/{subjectId}")]
        public IHttpActionResult UpdateSubject(int studentId, int subjectId, SubjectViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var subject = Mapper.Map<SubjectViewModel, Subject>(viewModel);
                var result = _subjectService.UpdateSubject(studentId, subjectId, subject);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("deleteSubject/{subjectId}")]
        public IHttpActionResult DeleteSubject(int subjectId)
        {
            try
            {
                var result = _subjectService.DeleteSubject(subjectId);
                return Content(result.IsSucceeded ? HttpStatusCode.OK :
                    HttpStatusCode.Conflict, result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
