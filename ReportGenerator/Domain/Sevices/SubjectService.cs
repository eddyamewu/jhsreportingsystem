﻿using ReportGenerator.Core;
using ReportGenerator.Data.Repository;
using ReportGenerator.Domain.Contracts;
using ReportGenerator.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReportGenerator.Domain.Sevices
{
    public class SubjectService : ISubjectService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SubjectService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Result<Subject> GetSubject(int subjectId)
        {
            try
            {
                if (subjectId < 1)
                    return new Result<Subject>(ResultType.Error,
                        "SubjectId not found");

                var subject = _unitOfWork.Subjects.GetById(subjectId);
                return new Result<Subject>(subject);
            }
            catch (Exception ex)
            {
                return new Result<Subject>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<IEnumerable<Subject>> GetSubjects()
        {
            try
            {
                var subjects = _unitOfWork.Subjects.GetAll();
                return new Result<IEnumerable<Subject>>(subjects);
            }
            catch (Exception ex)
            {
                return new Result<IEnumerable<Subject>>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<Subject> AddSubject(int studentId, Subject subject)
        {
            try
            {
                if (studentId < 1)
                    return new Result<Subject>(ResultType.Error,
                        "StudentId not found.");

                var student = _unitOfWork.Students.GetById(studentId);
                if (student == null)
                    return new Result<Subject>(ResultType.Error,
                        $"Student with Id: {studentId} not found.");

                subject.ExamPercentageScore = subject.ExamScore * 0.05;
                subject.TestPercentageScore = subject.TestScore * 0.05;
                subject.TotalScore = subject.ExamScore + subject.TestScore;
                subject.Grade = SetSubjectGrade(subject.TotalScore);
                subject.Remarks = SetSubjectRemark(subject.TotalScore);
                subject.TotalPercentageScore = subject.ExamPercentageScore + subject.TestPercentageScore;
                student.Subjects.Add(subject);
                _unitOfWork.SaveChanges();

                SetSubjectPosition(studentId, subject.Name);

                return new Result<Subject>(ResultType.Success,
                    "Subject saved successfully.");
            }
            catch (Exception ex)
            {
                return new Result<Subject>(ResultType.Unknown, ex.Message);
            }
        }



        public Result<Subject> DeleteSubject(int subjectId)
        {
            try
            {
                if (subjectId < 1)
                    return new Result<Subject>(ResultType.Error,
                        "SubjectId not found");

                var subject = _unitOfWork.Subjects.GetById(subjectId);
                if (subject == null)
                    return new Result<Subject>(ResultType.Error,
                        $"Student with Id: {subjectId} not found.");

                _unitOfWork.Subjects.Remove(subject);
                return new Result<Subject>(subject);
            }
            catch (Exception ex)
            {
                return new Result<Subject>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<Subject> UpdateSubject(int studentId, int subjectId, Subject subject)
        {
            try
            {
                if (subjectId < 1)
                    return new Result<Subject>(ResultType.Error,
                        "SubjectId not found");

                var originalSubject = _unitOfWork.Subjects.GetById(subjectId);
                if (originalSubject == null)
                    return new Result<Subject>(ResultType.Error,
                        $"Student with Id: {subjectId} not found.");

                originalSubject.Name = subject.Name;
                originalSubject.Teacher = subject.Teacher;
                originalSubject.ExamScore = subject.ExamScore;
                originalSubject.TestScore = subject.TestScore;
                originalSubject.ExamPercentageScore = originalSubject.ExamScore * 0.05;
                originalSubject.TestPercentageScore = originalSubject.TestScore * 0.05;
                originalSubject.TotalScore = originalSubject.ExamScore + originalSubject.TestScore;
                originalSubject.TotalPercentageScore = originalSubject.ExamPercentageScore + originalSubject.TestPercentageScore;
                _unitOfWork.SaveChanges();

                SetSubjectPosition(studentId, subject.Name);

                return new Result<Subject>(ResultType.Success,
                    "Subject updated successfully.");
            }
            catch (Exception ex)
            {
                return new Result<Subject>(ResultType.Unknown, ex.Message);
            }
        }

        private void SetSubjectPosition(int studentId, string subjectName)
        {
            var student = _unitOfWork.Students.GetById(studentId);
            var students = _unitOfWork.Students.GetAll()
                .SelectMany(stud => stud.Subjects
                .OrderByDescending(t => t.TotalPercentageScore),
                (stud, subject) => new { stud, subject })
                .Where(t => t.subject.Name == subjectName
                && t.stud.Form == student.Form)
                .Select(t => t.stud).ToList();

            foreach (var subject in students.SelectMany(studt => studt.Subjects))
            {
                subject.Position = students.IndexOf(student) + 1;
            }

            _unitOfWork.SaveChanges();
        }

        private static int SetSubjectGrade(double totalScore)
        {
            if (totalScore > 90) return 1;
            if (totalScore > 80) return 2;
            if (totalScore > 70) return 3;
            if (totalScore > 60) return 4;
            if (totalScore > 50) return 5;
            if (totalScore > 44) return 6;
            return 7;
        }

        private static string SetSubjectRemark(double totalScore)
        {
            if (totalScore > 90) return "Exceleent";
            if (totalScore > 80) return "Very Good";
            if (totalScore > 70) return "Good";
            if (totalScore > 60) return "Credit";
            if (totalScore > 50) return "Pass";
            if (totalScore > 44) return "Fair";
            return "Below Average";
        }
    }
}