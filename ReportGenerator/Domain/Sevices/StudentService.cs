using ReportGenerator.Core;
using ReportGenerator.Data.Repository;
using ReportGenerator.Domain.Contracts;
using ReportGenerator.Domain.Models;
using System;
using System.Collections.Generic;

namespace ReportGenerator.Domain.Sevices
{
    public class StudentService : IStudentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StudentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Result<Student> GetStudent(int studentId)
        {
            try
            {
                if (studentId < 1)
                    return new Result<Student>(ResultType.Error,
                        "StudentId not found");

                var student = _unitOfWork.Students.GetById(studentId);
                return new Result<Student>(student);
            }
            catch (Exception ex)
            {
                return new Result<Student>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<IEnumerable<Student>> GetStudents()
        {
            try
            {
                var students = _unitOfWork.Students.GetAll();
                return new Result<IEnumerable<Student>>(students);
            }
            catch (Exception ex)
            {
                return new Result<IEnumerable<Student>>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<Student> AddStudent(int schoolId, Student student)
        {
            try
            {
                if (schoolId < 1)
                    return new Result<Student>(ResultType.Error,
                        "SchoolId not found.");

                var school = _unitOfWork.Schools.GetById(schoolId);
                if (school == null)
                    return new Result<Student>(ResultType.Error,
                        $"School with Id: {schoolId} not found.");

                school.Students.Add(student);
                _unitOfWork.SaveChanges();
                return new Result<Student>(ResultType.Success,
                    "Student saved successfully.");
            }
            catch (Exception ex)
            {
                return new Result<Student>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<Student> DeleteStudent(int studentId)
        {
            try
            {
                if (studentId < 1)
                    return new Result<Student>(ResultType.Success,
                        "StudentId not found.");

                var student = _unitOfWork.Students.GetById(studentId);
                if (student == null)
                    return new Result<Student>(ResultType.Success,
                        $"Student with Id: {studentId} not found.");

                _unitOfWork.Students.Remove(student);
                _unitOfWork.SaveChanges();
                return new Result<Student>(ResultType.Success,
                    "Student deleted succesfully.");
            }
            catch (Exception ex)
            {
                return new Result<Student>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<Student> UpdateStudent(int studentId, Student student)
        {
            try
            {
                if (studentId < 1)
                    return new Result<Student>(ResultType.Success,
                        "StudentId not found.");

                var originalStudent = _unitOfWork.Students.GetById(studentId);
                if (originalStudent == null)
                    return new Result<Student>(ResultType.Success,
                        $"Student with Id: {studentId} not found.");

                originalStudent.Form = student.Form;
                originalStudent.AttendanceMade = student.AttendanceMade;
                originalStudent.Lastname = student.Lastname;
                originalStudent.Firstname = student.Firstname;
                originalStudent.Othername = student.Othername;
                originalStudent.TeacherRemarks = student.TeacherRemarks;
                originalStudent.AcademicConduct = student.AcademicConduct;
                originalStudent.AcademicAttitude = student.AcademicAttitude;
                originalStudent.AcademicInterest = student.AcademicInterest;
                originalStudent.AcademicStrength = student.AcademicStrength;
                originalStudent.AcademicWeakness = student.AcademicWeakness;
                originalStudent.HeadmasterRemarks = student.HeadmasterRemarks;
                _unitOfWork.SaveChanges();

                return new Result<Student>(ResultType.Success, "Student updated successfully.");
            }
            catch (Exception ex)
            {
                return new Result<Student>(ResultType.Unknown, ex.Message);
            }
        }
    }
}