﻿using ReportGenerator.Core;
using ReportGenerator.Data.Repository;
using ReportGenerator.Domain.Contracts;
using ReportGenerator.Domain.Models;
using System;
using System.Collections.Generic;

namespace ReportGenerator.Domain.Sevices
{
    public class SchoolService : ISchoolService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SchoolService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Result<School> GetSchool(int schoolId)
        {
            try
            {
                if (schoolId < 1)
                    return new Result<School>(ResultType.Error,
                        "SchoolId not found.");

                var school = _unitOfWork.Schools.GetById(schoolId);

                if (school == null)
                    return new Result<School>(ResultType.Error,
                        "SchoolId not found.");

                return new Result<School>(school,
                    $"School wth Id: {schoolId} found.");
            }
            catch (Exception ex)
            {
                return new Result<School>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<IEnumerable<School>> GetSchools()
        {
            try
            {
                var schools = _unitOfWork.Schools.GetAll();

                return new Result<IEnumerable<School>>(schools,
                    "Schools found.");
            }
            catch (Exception ex)
            {
                return new Result<IEnumerable<School>>(ResultType.Unknown, ex.Message);
            }

        }

        public Result<School> AddSchool(School school)
        {
            try
            {
                if (school == null)
                    return new Result<School>(ResultType.Error,
                        "School cannot be empty.");

                _unitOfWork.Schools.Add(school);
                _unitOfWork.SaveChanges();

                return new Result<School>(ResultType.Success,
                    "School saved successfully.");
            }
            catch (Exception ex)
            {
                return new Result<School>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<School> DeleteSchool(int schoolId)
        {
            try
            {
                if (schoolId < 1)
                    return new Result<School>(ResultType.Error,
                        "SchoolId cannot be empty.");

                var school = _unitOfWork.Schools.GetById(schoolId);
                if (school == null)
                    return new Result<School>(ResultType.Error,
                        $"School with Id: {schoolId} cannot be empty.");

                _unitOfWork.Schools.Remove(school);
                return new Result<School>(ResultType.Success,
                    $"School with Id: {schoolId} deleted successfullly.");
            }
            catch (Exception ex)
            {
                return new Result<School>(ResultType.Unknown, ex.Message);
            }
        }

        public Result<School> UpdateSchool(int schoolId, School school)
        {
            try
            {
                if (schoolId < 1)
                    return new Result<School>(ResultType.Error,
                        "SchoolId cannot be empty.");

                if (school == null)
                    return new Result<School>(ResultType.Error,
                        "School cannot be empty.");

                var originalSchool = _unitOfWork.Schools.GetById(schoolId);

                if (originalSchool == null)
                    return new Result<School>(ResultType.Error,
                        $"School with Id: {schoolId} not found.");

                originalSchool.Name = school.Name;
                originalSchool.Year = school.Year;
                originalSchool.Address = school.Address;
                originalSchool.Closing = school.Closing;
                originalSchool.Contact = school.Contact;
                originalSchool.Location = school.Location;
                originalSchool.Resumption = school.Resumption;
                originalSchool.TotalAttendance = school.TotalAttendance;

                _unitOfWork.SaveChanges();

                return new Result<School>(ResultType.Success,
                    $"School with Id: {schoolId} updated successfullly.");

            }
            catch (Exception ex)
            {
                return new Result<School>(ResultType.Unknown, ex.Message);

            }
        }
    }
}
