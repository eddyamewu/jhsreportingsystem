﻿using ReportGenerator.Core;
using ReportGenerator.Domain.Models;
using System.Collections.Generic;

namespace ReportGenerator.Domain.Contracts
{
    public interface ISubjectService
    {
        Result<Subject> GetSubject(int subjectId);
        Result<IEnumerable<Subject>> GetSubjects();
        Result<Subject> AddSubject(int studentId, Subject subject);
        Result<Subject> DeleteSubject(int subjectId);
        Result<Subject> UpdateSubject(int studentId, int subjectId, Subject subject);
    }
}
