﻿using ReportGenerator.Core;
using ReportGenerator.Domain.Models;
using System.Collections.Generic;

namespace ReportGenerator.Domain.Contracts
{
    public interface ISchoolService
    {
        Result<School> GetSchool(int schoolId);
        Result<IEnumerable<School>> GetSchools();
        Result<School> AddSchool(School school);
        Result<School> DeleteSchool(int schoolId);
        Result<School> UpdateSchool(int schoolId, School school);

    }
}
