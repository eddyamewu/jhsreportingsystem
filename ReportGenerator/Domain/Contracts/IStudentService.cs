﻿using ReportGenerator.Core;
using ReportGenerator.Domain.Models;
using System.Collections.Generic;

namespace ReportGenerator.Domain.Contracts
{
    public interface IStudentService
    {
        Result<Student> GetStudent(int studentId);
        Result<IEnumerable<Student>> GetStudents();
        Result<Student> AddStudent(int schoolId, Student student);
        Result<Student> DeleteStudent(int studentId);
        Result<Student> UpdateStudent(int studentId, Student student);
    }
}
