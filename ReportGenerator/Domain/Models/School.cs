using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReportGenerator.Domain.Models
{
    public class School
    {
        public School()
        {
            Created = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public int Year { get; set; }
        public int TotalAttendance { get; set; }
        public DateTime? Resumption { get; set; }
        public DateTime? Closing { get; set; }
        public DateTime Created { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}