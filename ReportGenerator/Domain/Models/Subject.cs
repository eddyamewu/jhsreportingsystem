using System;
using System.ComponentModel.DataAnnotations;

namespace ReportGenerator.Domain.Models
{
    public class Subject
    {
        public Subject()
        {
            Created = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public double TestPercentageScore { get; set; }
        public double ExamPercentageScore { get; set; }
        public double TotalPercentageScore { get; set; }
        public double TestScore { get; set; }
        public double ExamScore { get; set; }
        public double TotalScore { get; set; }
        public int Grade { get; set; }
        public int Position { get; set; }
        public string Remarks { get; set; }
        public string Teacher { get; set; }
        public DateTime Created { get; set; }
    }
}