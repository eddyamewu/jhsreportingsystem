﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReportGenerator.Domain.Models
{
    public class Student
    {
        public Student()
        {
            Created = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Othername { get; set; }
        public string Form { get; set; }
        public string Position { get; set; }
        public double AverageScore { get; set; }
        public double TotalScore { get; set; }
        public bool Promoted { get; set; }
        public int AttendanceMade { get; set; }
        public string AcademicWeakness { get; set; }
        public string AcademicStrength { get; set; }
        public string AcademicConduct { get; set; }
        public string AcademicAttitude { get; set; }
        public string AcademicInterest { get; set; }
        public string TeacherRemarks { get; set; }
        public string HeadmasterRemarks { get; set; }
        public DateTime Created { get; set; }
        public ICollection<Subject> Subjects { get; set; }
    }
}
