namespace ReportGenerator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadeSomePropsRequiredRemoved : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Schools", "Name", c => c.String());
            AlterColumn("dbo.Schools", "Contact", c => c.String());
            AlterColumn("dbo.Students", "Firstname", c => c.String());
            AlterColumn("dbo.Students", "Lastname", c => c.String());
            AlterColumn("dbo.Students", "Form", c => c.String());
            AlterColumn("dbo.Subjects", "Name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Subjects", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Students", "Form", c => c.String(nullable: false));
            AlterColumn("dbo.Students", "Lastname", c => c.String(nullable: false));
            AlterColumn("dbo.Students", "Firstname", c => c.String(nullable: false));
            AlterColumn("dbo.Schools", "Contact", c => c.String(nullable: false));
            AlterColumn("dbo.Schools", "Name", c => c.String(nullable: false));
        }
    }
}
