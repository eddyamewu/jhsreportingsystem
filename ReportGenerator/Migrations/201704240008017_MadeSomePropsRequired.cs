namespace ReportGenerator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadeSomePropsRequired : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "Position", c => c.String());
            AddColumn("dbo.Subjects", "TestPercentageScore", c => c.Double(nullable: false));
            AddColumn("dbo.Subjects", "ExamPercentageScore", c => c.Double(nullable: false));
            AddColumn("dbo.Subjects", "TotalPercentageScore", c => c.Double(nullable: false));
            AddColumn("dbo.Subjects", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Schools", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Schools", "Contact", c => c.String(nullable: false));
            AlterColumn("dbo.Students", "Firstname", c => c.String(nullable: false));
            AlterColumn("dbo.Students", "Lastname", c => c.String(nullable: false));
            AlterColumn("dbo.Students", "Form", c => c.String(nullable: false));
            AlterColumn("dbo.Subjects", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Subjects", "Name", c => c.String());
            AlterColumn("dbo.Students", "Form", c => c.String());
            AlterColumn("dbo.Students", "Lastname", c => c.String());
            AlterColumn("dbo.Students", "Firstname", c => c.String());
            AlterColumn("dbo.Schools", "Contact", c => c.String());
            AlterColumn("dbo.Schools", "Name", c => c.String());
            DropColumn("dbo.Subjects", "Created");
            DropColumn("dbo.Subjects", "TotalPercentageScore");
            DropColumn("dbo.Subjects", "ExamPercentageScore");
            DropColumn("dbo.Subjects", "TestPercentageScore");
            DropColumn("dbo.Students", "Position");
        }
    }
}
