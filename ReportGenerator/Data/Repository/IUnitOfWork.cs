﻿using ReportGenerator.Data.Repository.Contracts;
using System;

namespace ReportGenerator.Data.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        ISchoolRepository Schools { get; }
        ISubjectRepository Subjects { get; }
        IStudentRepository Students { get; }
        int SaveChanges();
    }
}
