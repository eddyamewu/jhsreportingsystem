﻿using ReportGenerator.Data.Repository.Contracts;
using ReportGenerator.Domain.Models;
using System.Data.Entity;

namespace ReportGenerator.Data.Repository.Repository
{
    public class SubjectRepository : Repository<Subject>, ISubjectRepository
    {
        public SubjectRepository(DbContext context) : base(context) { }
        public ApplicationDbContext ApplicationDbContext => Context as ApplicationDbContext;

    }
}
