﻿using ReportGenerator.Data.Repository.Contracts;
using ReportGenerator.Domain.Models;

namespace ReportGenerator.Data.Repository.Repository
{
    public class SchoolRepository : Repository<School>, ISchoolRepository
    {
        public SchoolRepository(ApplicationDbContext context) : base(context) { }
        public ApplicationDbContext ApplicationDbContext => Context as ApplicationDbContext;
    }
}
