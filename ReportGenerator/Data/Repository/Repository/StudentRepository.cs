﻿using System.Data.Entity;
using ReportGenerator.Data.Repository.Contracts;
using ReportGenerator.Domain.Models;

namespace ReportGenerator.Data.Repository.Repository
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        public StudentRepository(DbContext context) : base(context) { }
        
        public  ApplicationDbContext ApplicationDbContext => Context as ApplicationDbContext;
    }
}
