﻿using ReportGenerator.Domain.Models;

namespace ReportGenerator.Data.Repository.Contracts
{
    public interface IStudentRepository : IRepository<Student>
    {

    }
}
