﻿using ReportGenerator.Domain.Models;

namespace ReportGenerator.Data.Repository.Contracts
{
    public interface ISchoolRepository : IRepository<School>
    {

    }
}
