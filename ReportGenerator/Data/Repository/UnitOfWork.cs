﻿using ReportGenerator.Data.Repository.Contracts;
using ReportGenerator.Data.Repository.Repository;

namespace ReportGenerator.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Schools = new SchoolRepository(_context);
            Subjects = new SubjectRepository(_context);
            Students = new StudentRepository(_context);

        }

        public ISchoolRepository Schools { get; }
        public ISubjectRepository Subjects { get; }
        public IStudentRepository Students { get; }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
