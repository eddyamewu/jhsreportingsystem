﻿namespace ReportGenerator.Core
{
    public class Result<TEntity> where TEntity : class
    {
        public Result() { }
        public Result(ResultType resultType, string message, TEntity value)
        {
            Set(resultType, message, value);
        }
        public Result(ResultType resultType, string message)
        {
            Set(resultType, message, null);
        }
        public Result(TEntity value, string message)
        {
            Set(ResultType.Success, message, value);
        }
        public Result(TEntity value)
        {
            Set(ResultType.Success, null, value);
        }

        public Result(ResultType resultType, TEntity value)
        {
            Set(resultType, null, value);
        }

        private void Set(ResultType resultType, string message, TEntity value)
        {
            Data = value;
            Message = message;
            IsSucceeded = resultType == ResultType.Success;
            HasValue = IsSucceeded && GenericTypeHelper<TEntity>.GetHasValue(value);
        }

        public TEntity Data { get; private set; }
        public bool HasValue { get; private set; }
        public bool IsSucceeded { get; private set; }
        public string Message { get; private set; }
    }
}
