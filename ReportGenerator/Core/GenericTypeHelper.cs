using System.Collections;

namespace ReportGenerator.Core
{
    public class GenericTypeHelper<TEntity>
    {
        public static bool GetHasValue(TEntity value)
        {
            if (typeof(TEntity).IsValueType) return true;

            if (Equals(value, null)) return false;

            var enumerable = value as IEnumerable;

            return enumerable == null || enumerable.GetEnumerator().MoveNext();
        }
    }
}